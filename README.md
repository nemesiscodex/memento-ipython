## Memento

Code for the paper titled "Automatic Classification of Source Code Archives by Programming Language: A Deep Learning Approach"

Check [notebook](https://gitlab.com/nemesiscodex/memento-ipython/blob/master/Memento.ipynb)

### Usage

#### Install Requirements

```bash
pip install -r requirements.txt
```

#### Run notebook
```
jupyter notebook Memento.ipynb
```

#### Datasets (Rosetta, github, lang)

[datasets](https://mega.nz/fm/moRi3BJS)


##### Best results on Rosetta:
<table>
 <tr>
  <th>Embeddings Size</th>
  <th>Sequence Length</th>
  <th>Dropout</th>
  <th>Vocabulary Size</th>
  <th>Accuracy</th>
  <th>Loss</th>
  <th>F1-score macro</th>
  <th>F1-score micro</th>
 </tr>
 <tr>
  <td>256</td>
  <td>250</td>
  <td>0.5</td>
  <td>28000</td>
  <td>0.8493</td>
  <td>0.6807</td>
  <td>0.7073</td>
  <td>0.8480</td>
 </tr>
</table>

